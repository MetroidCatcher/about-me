# There used to be a Twitter link, to my profile (WIP)
Note: Anyone who claims to be **MetroidCatcher**, unless stated otherwise by me, is fake. **!! DO NOT FOLLOW THEM !!**<br>
***~~I reclaimed my @ over there, but it'll remain inactive, just to prevent the uber troll or other weirdos to take it and do weird things with it. I rather see it suspended than used by anyone else.~~ [He can change it anyways... there's that....](https://www.npr.org/2023/07/29/1190891082/twitter-x-account-owner-gene-hwang-elon-musk) ([Techcrunch article](https://techcrunch.com/2023/07/26/twitter-now-x-took-over-the-x-handle-without-warning-or-compensating-its-owner/), [Business Insider article, archived](https://archive.is/CZnYp))***<br><br>
I've created a placeholder account under this name; [Nitter does not work anymore](https://github.com/zedeus/nitter/issues/1155#issuecomment-1913361757) and as a logged-out user, there is basically nothing you can do much.<br>
As a less-privacy-intrusive thing, I use this addon called **Old Twitter**. ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/old-twitter-layout-2022/), [Chrome](https://chrome.google.com/webstore/detail/old-twitter-layout-2022/jgejdcdoeeabklepnkdbglgccjpdgpmf))<br>

First and foremost, this text may be seen from some as passive aggressive. It may is (depends who reads it, I guess. Hello, fragile egos out there. Have some self-awareness, thanks.), but I'm being respectful here, not swearing or anything.

### *PLEASE TAKE THESE WITH A GRAIN OF SALT, THANK YOU.*

### *If I'm wrong, then correct me please. Do this in a respectful manner, unless I'll just ignore you.*

## Very short version why I left Twitter
Elon Musk does weird and nonsensical changes to it; spreads (far from reality) [weird](https://www.politifact.com/factchecks/2024/feb/23/elon-musk/elon-musk-says-theres-scientific-consensus-on-birt/)¹ [conspiracy](https://www.politifact.com/factchecks/2024/feb/06/elon-musk/elon-musk-is-wrong-to-say-joe-biden-is-recruiting/)² [theories](https://www.politifact.com/factchecks/2022/may/04/elon-musk/elon-musks-false-claim-nbc-news-covered-hunter-bid/)³. [Those](https://archive.is/XTCes)⁴ [ones](https://archive.is/QWHII)⁵ ([Article](https://www.politifact.com/factchecks/2024/mar/06/elon-musk/fact-check-elon-musks-claim-that-democrats-are-avo/)), for example.
## Long version why I left Twitter
- He is making the Platform [worse for normal, non-paying users](https://web.archive.org/web/20230601201351/https://memeburn.com/2023/04/twitter-prioritizes-verified-subscribers/)
- [Removing features that was previously free](https://archive.is/ZA1BP), requires now a Twitter Blue subscription
- Twitter Blue aka „verification” fiasco itself, creating exactly this [„Lords and peasants”-system](https://archive.is/wau8Z), aka it is more corrupted than it was before
- [The algorithm is pushing those, who are paying](https://web.archive.org/web/20240409212830/https://www.theverge.com/2022/11/1/23435092/elon-musk-twitter-blue-verification-cost-ads-search) for the blue checkmark ([Wikipedia article](https://en.wikipedia.org/wiki/Twitter_Blue_verification_controversy), [Forbes article](https://www.forbes.com/sites/johnbbrandon/2023/01/20/heres-why-twitter-blue-is-a-complete-waste-of-time/), [The Ringer article](https://www.theringer.com/tech/2023/4/27/23700212/elon-musk-twitter-blue-check-mark-subscription-political-symbol), [The Guardian article](https://www.theguardian.com/technology/2022/nov/01/musk-charging-twitter-verified-accounts))
- [Also the algorithm is pushing Elon Musk tweets by default](https://web.archive.org/web/20240215101614/https://www.businessinsider.com/musk-changed-twitter-algorithm-tweets-didnt-get-attention-book-2024-2) (Cause he don't have enough reach by default, before these big brain changes, huh... [Also, his narcissism. (2018 article)](https://www.imd.org/research-knowledge/leadership/articles/will-elon-musks-narcissism-be-his-downfall/))
- [Elon Reeve Musk (53), acts like he is not an adult](https://www.theatlantic.com/newsletters/archive/2022/12/the-childish-drama-of-elon-musk/672496/) (How about to be a *real* man, and maybe don't cry and throw around tantrums all day long, hm? Man up already. I don't mind if you do, when do you play Video Games and whatnot, but this? Come on. This does not belong in politics or running a company here, ~~or being in the US gov...~~ You should know better. *DO* better. This ain't a circus.)

## *Of course, this isn't a complete list, gathering more good, reliable sources.*

***I'll still deadname X Twitter, like Elon does with Vivian. Deal with it. All hail true free speech.***